package com.roamtech.emalipay;

import androidx.multidex.MultiDexApplication;

import com.fxn.stash.Stash;

public class App extends MultiDexApplication {

    private static App mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        Stash.init(this);
    }
}
