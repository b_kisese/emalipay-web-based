package com.roamtech.emalipay.fcm;

import android.content.Context;

import com.fxn.stash.Stash;

public class FirebaseKeyStore {
    public FirebaseKeyStore(Context context) {
    }

    public void storeFireBaseKey(String key) {
        Stash.put("FIREBASE_KEY", key);
    }

    public String getFireBaseKey() {
        return Stash.getString("FIREBASE_KEY");
    }
}
