package com.roamtech.emalipay;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.roamtech.emalipay.api.ApiClient;
import com.roamtech.emalipay.api.model.FireBaseReg;
import com.roamtech.emalipay.fcm.FirebaseKeyStore;

import im.delight.android.webview.AdvancedWebView;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity implements AdvancedWebView.Listener{

    private AdvancedWebView mWebView;
    //Retrofit API Client
    public ApiClient mApiClient;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTheme(R.style.Theme_EmaliPay);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        mWebView = (AdvancedWebView) findViewById(R.id.webview);
        imageView = (ImageView)findViewById(R.id.imageview);
        mWebView.setListener(this, this);
        mWebView.setMixedContentAllowed(false);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.loadUrl("https://emalipay.tk/");
    }


    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebView.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onBackPressed() {
        if (!mWebView.onBackPressed()) { return; }
        // ...
        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        imageView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageFinished(String url) {
        imageView.setVisibility(View.GONE);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }
}